import 'package:flutter/material.dart';


class DashboardWidget extends StatefulWidget {
  const DashboardWidget({Key? key}) : super(key: key);

  @override
  _DashboardWidget createState() => _DashboardWidget();
}

class _DashboardWidget extends State<DashboardWidget> {

  static const String _title = 'Module 4 - Dashboard';
  @override
  Widget build(BuildContext context) {
    return  Scaffold
        (
        appBar: AppBar
          (
          title: const Text(_title),

        ),
        body: Container(
          padding: const EdgeInsets.symmetric(
              vertical: 20.0,
              horizontal: 2.0),
          child: GridView.count(
            crossAxisCount: 2,
            padding: const EdgeInsets.all(3.0),
            children: <Widget>[
              makeDashboardItem("New Measurement", Icons.addchart),
              makeDashboardItem("View history", Icons.history),
              makeDashboardItem("Search", Icons.search),
              makeDashboardItem("Edit items", Icons.edit),
              makeDashboardItem("Settings", Icons.settings),
              makeDashboardItem("Exit", Icons.exit_to_app)
            ],
          ),
        ),
    );
  }

  Card makeDashboardItem(String title, IconData icon)
  {
    return Card(
      elevation: 1.0,
      margin:const  EdgeInsets.all(8.0),
      child: Container(
        child: InkWell(
          onTap: (){},
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            mainAxisSize: MainAxisSize.min,
            verticalDirection: VerticalDirection.down,
            children: <Widget>[
              const SizedBox(height: 50.0),
              Center(
                child: Icon(
                  icon,
                  size: 50.0,
                  //color: Colors.black,
                ),
              ),
              const SizedBox(height: 50.0),
              Center(
                child: Text(title,
                  style: const TextStyle(fontSize: 18.0, color: Colors.black),),
              )
            ],

          ),
        ),

      ),
    );
  }
}

