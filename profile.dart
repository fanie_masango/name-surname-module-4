import 'Dashboard.dart';
import 'package:flutter/material.dart';

class Profile extends StatelessWidget{
  const Profile({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        title: const Text("Profile"),
        centerTitle: true,
      ),
      body: Column(
        children: [
          SizedBox(
            height: 115,
            width: 115,
            child: Stack(
              fit: StackFit.expand,
              clipBehavior: Clip.none ,
              children: const [
                   CircleAvatar(
                  backgroundImage: AssetImage("assets/b1.jpg") ,
                )
              ],
            ),
          ),
          const Padding(
              padding:  EdgeInsets.all(8.0),
            child:  TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Username',
              ),
            ),
          ),
          const Padding(
            padding:  EdgeInsets.all(8.0),
            child:  TextField(
              obscureText: true,
              decoration: InputDecoration(
                border: OutlineInputBorder(),
                labelText: 'Password',
              ),
            ),
          )

        ],
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: (){Navigator.push(context, MaterialPageRoute(builder: (context) => const DashboardWidget()),);},
        child: const Icon(Icons.start),
      ),
    );
  }

}