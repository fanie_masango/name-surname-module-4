import 'package:flutter/material.dart';
import 'profile.dart';
import 'package:splashscreen/splashscreen.dart';
void main(){
  runApp( MaterialApp(
    home: MyApp(),
    theme:

        ThemeData(
          colorScheme:
            ColorScheme.fromSwatch(
              primarySwatch: Colors.cyan).copyWith(secondary: Colors.yellowAccent),
              cardColor: Colors.cyan,
              primaryColor: Colors.cyan,
              unselectedWidgetColor: Colors.lightGreen,
              backgroundColor: Colors.cyan,
        )


    ),
  );
}


class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() =>  _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return  SplashScreen(
        seconds: 4,
        navigateAfterSeconds: const Profile(),
        title: const Text('WELCOME',
          style: TextStyle(
              fontWeight: FontWeight.bold,
              fontSize: 20.0
          ),),
        image: Image.asset("assets/cc1.png"
        ),
        styleTextUnderTheLoader: const TextStyle(),
        photoSize: 100.0,
    );

  }
}